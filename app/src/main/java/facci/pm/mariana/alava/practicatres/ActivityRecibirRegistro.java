package facci.pm.mariana.alava.practicatres;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

public class ActivityRecibirRegistro extends AppCompatActivity {

    private TextView Registro;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recibir_registro);

        setTitle(getString(R.string.registrar));

        Registro = (TextView)findViewById(R.id.LBLRegistro);

        Bundle bundle = this.getIntent().getExtras();
        Registro.setText(bundle.getString("nombres")+ "\n"+bundle.getString("apellidos") +"\n"+ bundle.getString("cedula")
                +"\n"+bundle.getString("correo")+"\n"+bundle.getString("clave"));


    }
}
