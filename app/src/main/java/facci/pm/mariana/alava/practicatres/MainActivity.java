package facci.pm.mariana.alava.practicatres;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity implements View.OnClickListener{

    private Button login, registro, buscar, pasarParametro;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        login = (Button)findViewById(R.id.BTNLogin);
        registro = (Button)findViewById(R.id.BTNRegistrar);
        buscar = (Button)findViewById(R.id.BTNBuscar);
        pasarParametro = (Button)findViewById(R.id.BTNParametro);

        login.setOnClickListener(this);
        registro.setOnClickListener(this);
        buscar.setOnClickListener(this);
        pasarParametro.setOnClickListener(this);



    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        Intent intent;

        switch (item.getItemId()){
            case R.id.opcionLogin:
                intent = new Intent(MainActivity.this, ActivityLogin.class);
                startActivity(intent);
                break;

            case R.id.opcionRegistrar:
                intent = new Intent(MainActivity.this, ActivityRegistrar.class);
                startActivity(intent);
                break;

            case R.id.opcionParametro:
                intent = new Intent(MainActivity.this, ActivityPasarParametro.class);
                startActivity(intent);
                break;

            case R.id.opcionBuscar:
                intent = new Intent(MainActivity.this, ActivityBuscar.class);
                startActivity(intent);
                break;
        }
        return true;
    }



    @Override
    public void onClick(View v) {

        switch (v.getId()){
            case R.id.BTNBuscar:
                startActivity(new Intent(this, ActivityBuscar.class));
                break;

            case R.id.BTNLogin:
                startActivity(new Intent(this, ActivityLogin.class));
                break;

            case R.id.BTNRegistrar:
                startActivity(new Intent(this, ActivityRegistrar.class));
                break;

            case R.id.BTNParametro:
                startActivity(new Intent(this, ActivityPasarParametro.class));
                break;
        }
    }
}
