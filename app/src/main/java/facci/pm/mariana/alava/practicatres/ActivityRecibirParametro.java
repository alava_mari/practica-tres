package facci.pm.mariana.alava.practicatres;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

public class ActivityRecibirParametro extends AppCompatActivity {

    private TextView Dato;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recibir_parametro);
        setTitle(getString(R.string.recibir));

        Dato = (TextView)findViewById(R.id.LBLRecibir);
        Bundle bundle = this.getIntent().getExtras();
        Dato.setText(bundle.getString("dato"));

    }
}
