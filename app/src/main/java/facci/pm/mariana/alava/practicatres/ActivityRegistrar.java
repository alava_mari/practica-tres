package facci.pm.mariana.alava.practicatres;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class ActivityRegistrar extends AppCompatActivity {

    private EditText nombres, apellidos, cedula, correo, clave;
    private Button RegistroE;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registrar);
        setTitle(getString(R.string.registrar));

        nombres = (EditText)findViewById(R.id.TXTNombres);
        apellidos = (EditText)findViewById(R.id.TXTApellidos);
        cedula = (EditText)findViewById(R.id.TXTCedula);
        correo = (EditText)findViewById(R.id.TXTCorreo);
        clave = (EditText)findViewById(R.id.TXTClave);

        RegistroE = (Button)findViewById(R.id.BTNRegistrarEnviar);

        RegistroE.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (nombres.getText().toString().isEmpty()|| apellidos.getText().toString().isEmpty()| cedula.getText().toString().isEmpty()
                        || correo.getText().toString().isEmpty()|| clave.getText().toString().isEmpty()){
                    Toast.makeText(ActivityRegistrar.this, "Campos Necesarios", Toast.LENGTH_SHORT).show();
                }else {
                    Intent intent = new Intent(ActivityRegistrar.this, ActivityRecibirRegistro.class);
                    Bundle bundle = new Bundle();
                    bundle.putString("nombre", nombres.getText().toString());
                    bundle.putString("apellidos", apellidos.getText().toString());
                    bundle.putString("cedula", cedula.getText().toString());
                    bundle.putString("correo", correo.getText().toString());
                    bundle.putString("clave", clave.getText().toString());
                    intent.putExtras(bundle);
                    startActivity(intent);
                    nombres.setText("");
                    apellidos.setText("");
                    cedula.setText("");
                    correo.setText("");
                    clave.setText("");
                }
            }
        });

    }
}
